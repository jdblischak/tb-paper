---
title: Title
author: Author
date: Date
output: word_document
---

# Heading 1

*  Compact
*  Compact
*  Compact

## Heading 2

> Block quote

### Heading 3

```{r Normal, echo=FALSE, results='asis'}
library("stringi")
set.seed(12345)
cat(stri_rand_lipsum(3), sep="\n\n")
```

1. This bibliography has the style changed so that the there is a hanging indent of 0.25 inches. Other than that it is based on the Normal style.
