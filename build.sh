#!/bin/bash

# For some reason, Mendeley outputs Benjamini & Hochberg, 1995 as just
# 1995. I couldn't find any reason why it would not be output as
# Benjamini1995 in the same style as all the other references. At
# first I was able to reference the paper using @1995, but that no
# longer works. The commands below replace the 1995 with
# Benjamini1995.
sed -i 's/article{1995/article{Benjamini1995/' refs.bib

# Render Rmd to Word
Rscript -e 'rmarkdown::render("tb-paper.md")'

# Alternative build option

# RStudio version of pandoc was creating Word documents that could not be
# opened with Libre Office. Updating RStudio to the latest preview version
# fixed it. In case that happens again, below is an alternative build.

# Knit Rmd to md
# Rscript -e 'knitr::knit("tb-paper.Rmd")'

# Render from md to Word
# pandoc tb-paper.md --to docx --from markdown+autolink_bare_uris+ascii_identifiers+tex_math_single_backslash-implicit_figures --output tb-paper.docx --filter pandoc-citeproc --highlight-style tango --bibliography refs.bib
