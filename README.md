# Mycobacterial infection induces a specific human innate immune response

This repo contains the source files for the following manuscript:

> Blischak, J. D. et al. Mycobacterial infection induces a specific human innate immune response. Sci. Rep. 5, 16882; doi: 10.1038/srep16882 (2015).

Other links related to this publication:

*  [Pre-print](http://biorxiv.org/content/early/2015/04/03/017483)
*  [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/26586179)
*  [Journal](http://www.nature.com/articles/srep16882)
*  [GEO](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE67427)
*  [Code repository](https://bitbucket.org/jdblischak/tb)
*  [Data repository](https://bitbucket.org/jdblischak/tb-data)

## Description

The paper was written in Markdown using the RStudio IDE and rendered to a Word document using the R package [rmarkdown](http://rmarkdown.rstudio.com/).

## License

This content is available via the [CC BY 3.0 license](https://creativecommons.org/licenses/by/3.0/).
