*  Remove indentation of author list and affiliations.
*  Add page break to separate title page from rest of text.
*  Add two 12-pt font lines between title and author list, two between author list and affiliations, and one between affiliations and correspondence.
*  Place References after Methods.
*  Add page numbers.
*  Make sure figures and tables are displayed nicely.
*  Add page break between tables and figures in Supplementary Information.
