# Manuscript data

## Subject Terms

*  Biological sciences/Immunology/Gene regulation in immune cells/Immunogenetics
*  Biological sciences/Microbiology/Pathogens
*  Biological sciences/Genetics/Gene expression
*  Biological sciences/Systems biology/Bayesian inference

## Techniques

*  Gene expression profiling
*  RNA sequencing
*  Tissue culture

## Shortened abstract

The innate immune system provides the first response to pathogen infection and orchestrates the activation of the adaptive immune system.
Though a large component of the innate immune response is common to all infections, pathogen-specific responses have been documented.
The innate immune response is thought to be especially critical for fighting infection with Mycobacterium tuberculosis (MTB), the causative agent of tuberculosis (TB).
While TB can be deadly, only 5-10% of individuals infected with MTB develop active disease.
TB susceptibility is, at least partly, heritable.
Studies of variation in the innate immune response to MTB infection may therefore shed light on the genetic basis of TB susceptibility.
Yet we still do not know which properties of the innate immune response are specific to MTB infection and which represent a general response.
To begin addressing this gap, we infected macrophages with eight different bacteria, including different MTB strains and related mycobacteria, and studied their transcriptional response.
Although the gene regulatory responses were largely consistent across the infections, we identified a novel subset of genes whose regulation was affected specifically by infection with mycobacteria.
Genetic variants associated with regulatory differences in these genes should be considered candidate loci for explaining TB susceptibility.
